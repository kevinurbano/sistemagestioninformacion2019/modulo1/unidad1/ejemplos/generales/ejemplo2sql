﻿DROP DATABASE IF EXISTS b20190529;
CREATE DATABASE IF NOT EXISTS b20190529;
USE b20190529;

CREATE TABLE clientes(
    dni varchar(9),
    nombre varchar(100),
    apellido varchar(100),
    fechaNac date,
    telefono varchar(15),
    PRIMARY KEY(dni)
  );

CREATE TABLE productos(
    codigo int AUTO_INCREMENT,
    nombre varchar(100),
    precio float,
    PRIMARY KEY(codigo)
  );

CREATE TABLE compran(
    -- campos    
    dniCliente varchar(9),
    codigoProducto int,
    -- Claves
    PRIMARY KEY(dniCliente,codigoProducto),
    -- Claves Ajenas
    CONSTRAINT fkCompranClientes FOREIGN KEY (dniCliente)
      REFERENCES clientes(dni) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fkCompranProductos FOREIGN KEY (codigoProducto)
      REFERENCES productos(codigo) ON DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE proveedores(
    nif varchar(9),
    nombre varchar(100),
    direccion varchar(100),
    PRIMARY KEY(nif)
  );

CREATE TABLE suministran(
    codProducto int,
    nifProveedor varchar(9),
    PRIMARY KEY (codProducto,nifProveedor),
    UNIQUE KEY(codProducto),
    CONSTRAINT fkSuministranProductos FOREIGN KEY (codProducto)
      REFERENCES productos(codigo) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fkSuminitranProveedores FOREIGN KEY (nifProveedor)
      REFERENCES proveedores(nif) ON DELETE CASCADE ON UPDATE CASCADE    
  );